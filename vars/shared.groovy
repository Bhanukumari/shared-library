def call (stageName){
if ("${stageName}" == 'build'){
sh 'mvn clean'
sh 'mvn install'
}
else if ("${stageName}" == 'static code analysis'){
    sh 'mvn checkstyle:checkstyle'
}
else if ("${stageName}" == 'Bug-analysis'){
 sh 'mvn clean verify sonar:sonar \
  -Dsonar.projectKey=shared-library \
  -Dsonar.host.url=http://3.145.211.18:9000 \
  -Dsonar.login=sqp_20245d864925530e14a2b09212f7f77ef884a965'

}
else if ("${stageName}" == 'test'){
sh 'mvn clean org.jacoco:jacoco-maven-plugin:0.8.8:prepare-agent  test org.jacoco:jacoco-maven-plugin:0.8.8:report'
}
else if ("${stageName}" == 'create artifact'){
sh 'mvn package'
}
}
